// Mock database
// let posts = [];


// The "then" method capture the "Response" object and returns another promise which will eventually be resolved or rejected
fetch('https://jsonplaceholder.typicode.com/posts/')
//The first ".then" method from the "Response" object to convert the data retrieved into JSOn format to be used in our application
.then((response)=> response.json())
//Show converted JSON value from the "fetch requet"
.then((data) => showPosts(data));

//Posts ID
let count = 1;

//ADD POST DATA
document.querySelector("#form-add-post").addEventListener("submit", (e)=>{
  // Prevents the page from reloading
  // prevents default behavior of event
  e.preventDefault();
  fetch("https://jsonplaceholder.typicode.com/posts",{
  method: 'POST',
  body: JSON.stringify({
    title: document.querySelector('#txt-title').value ,  
    body:  document.querySelector('#txt-body').value,
    userID: 1
  }),
  // Sets the header data of the "request" data 
  headers:{
    'Content-type': 'application/json; charset=UTF-8'}
})
.then((response)=>response.json())
.then((data)=> {
  console.log(data);
  alert('Succefully added!');
  document.querySelector('#txt-title').value = null;
  document.querySelector('#txt-body').value = null;
})

  // posts.push({
  //   id: count,
  //   title: document.querySelector("#txt-title").value,
  //   body: document.querySelector("#txt-body").value,
  // });
  // count++
  // console.log(posts);
  // alert("Post Succefully Added!");
  
  // showPosts();
});

// RETRIEVE POSTS
const showPosts = (posts) => {
  // "postEntries" - variable that will contain all the posts
  let postEntries = "";

  posts.forEach((post) => {
      // "+=" - adds tha value of the right operand to a variable and assigns the result to the variable
      // "div" - division on where to place the value of title and posts
      postEntries += `
          <div id ="post-${post.id}">
              <h3 id="post-title-${post.id}">${post.title}</h3>
              <p id="post-body-${post.id}">${post.body}</p>
              <button onClick="editPost('${post.id}')">Edit</button>
              <button onClick="deletePost('${post.id}')">Delete</button>
          </div>
      `
  });
  // To check what is stored in the properties of variables
  //console.log(postEntries);
  document.querySelector("#div-post-entries").innerHTML = postEntries;

};


//EDIT POST

const editPost = (id) =>{
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;
  
  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;
  //To remove the "disabled" attribute from the update button
  // removeAttributes() - removes the attribute with the specified name from the element
  document.querySelector("#btn-submit-update").removeAttribute('disabled');
};
// UPDATE POST
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
  e.preventDefault();
  
  fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: 'PUT',
  body: JSON.stringify({
    id: document.querySelector('#txt-edit-id').value ,  
    title: document.querySelector('#txt-edit-title').value ,  
    body:  document.querySelector('#txt-edit-body').value,
    userID: 1
  }),
  // Sets the header data of the "request" object to be sent to the backend
  headers:{
    'Content-type': 'application/json; charset=UTF-8'}
})
.then((response)=>response.json())
.then((data)=> {
  console.log(data);
  alert('Succefully updated!');
  //Reset the edit post form input fields
  document.querySelector('#txt-edit-id').value = null;
  document.querySelector('#txt-edit-title').value = null;
  document.querySelector('#txt-edit-body').value = null;
  // .setAttribute - set an attribute to an HTML element
  document.querySelector('#btn-submit-update').setAttribute('disabled', true);
  
})
  // for (let i = 0; i < posts.length; i++) {

  //     if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
  //         posts[i].title = document.querySelector("#txt-edit-title").value;
  //         posts[i].body = document.querySelector("#txt-edit-body").value;

  //         showPosts(posts);
  //         alert("Successfully updated!");

  //         break;
  //     }
  // }
});


// // Delete POST
// const deletePost = (id) => {
//   const delPost = posts.findIndex(post => { return post.id == id })
//   posts.splice(delPost,1);
//   showPosts();
//   alert("Successfully deleted")
// };

//DELETE POST
const deletePost = (id) => {
  // Loops through all elements of the array to find the post ID that matches the post selected
  fetch(`https://jsonplaceholder.typicode.com/posts/1${id}`,{
  method: 'DELETE',})
  // posts = posts.filter((post) => {
  //     if (post.id.toString() !== id) {
  //         return post;
  //     }
  // });
  // The Element.remove() method removes the element from the DOM.
  document.querySelector(`#post-${id}`).remove();
  

};
